import React, { Component } from 'react'

import Home from './screens/home';
import Login from './screens/login';
// import Home from './screens/home';

import { BrowserRouter as Router, Route } from "react-router-dom";

export default class Routes extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={Login} />
                    <Route path="/login" component={Login} />
                    <Route path="/home" component={Home} />
                </div>
            </Router>
        )
    }
}
