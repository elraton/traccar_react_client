import React, { Component } from 'react';
import './App.css';
import Routes from './routes';

class App extends Component {

  componentDidMount() {
    localStorage.setItem('baseurl', 'http://laisladelraton.com:8080/api')
    
  }
  render() {
    return (
      <div className="App container-fluid">
        <Routes></Routes>
      </div>
    );
  }
}

export default App;
