import React, { Component } from 'react'
import OlMap from 'ol/Map'
import OlXYZ from 'ol/source/XYZ'
import OlTileLayer from 'ol/layer/Tile'
import OlView from 'ol/View'

import Feature from 'ol/Feature'
import Map from 'ol/Map'
import View from 'ol/View'
import Point from 'ol/geom/Point'
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer'
import {fromLonLat} from 'ol/proj'
import TileJSON from 'ol/source/TileJSON'
import VectorSource from 'ol/source/Vector'
import {Icon, Style, Text, Fill} from 'ol/style'

export default class Mapr extends Component {

    state = {
        map: null,
        vectorLayer: null,
        firstPosition: null
    }

    source= null
    layer= null
    view= null
    vectorSource= null

    constructor(props) {
        super(props)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.positions.length > 0) {
            this.firstPosition = this.props.positions[0];

            let cars = [];
      
            if (this.props.deviceId !== 0) {
                this.firstPosition = this.props.positions.find(x => x.deviceId == this.props.deviceId);
                this.state.map.setView(
                    new View({
                        center: fromLonLat([this.firstPosition.longitude, this.firstPosition.latitude]),
                        zoom: 14
                    })
                );
            }
            
            for (const xx of this.props.positions) {
                const device = this.props.devices.find(x => x.id == xx.deviceId);
                let color = '#FF0000';
                if (device.status != 'offline') {
                    color = '#00FF00';
                }
                const car = new Feature({
                    geometry: new Point(fromLonLat([xx.longitude, xx.latitude]))
                });
                
                car.setStyle(new Style({
                    image: new Icon( ({
                        color: color,
                        crossOrigin: 'anonymous',
                        src: '/assets/img/car_2.png'
                    })),
                    
                    text: new Text({
                        text: device.name,
                        offsetY: -25,
                        scale: 2,
                        fill: new Fill({
                            color: '#000'
                        }),
                        backgroundFill: new Fill({
                            color: '#fff'
                        })
                    })
                }));
                cars.push(car);
            }

            this.vectorSource = new VectorSource({
                features: cars
            });

            this.state.vectorLayer.setSource(this.vectorSource);
          }
    }

    componentDidMount() {
        this.source = new OlXYZ({
            url: 'http://tile.osm.org/{z}/{x}/{y}.png'
        });
      
        this.layer = new OlTileLayer({
            source: this.source
        });
      
        this.vectorSource = new VectorSource({
            features: []
        });

        const vectorLayer = new VectorLayer({
            source: this.vectorSource
        });

        this.setState({
            vectorLayer: vectorLayer,
            map: new Map({
                layers: [this.layer, vectorLayer],
                target: 'map',
                view: new View({
                    center: fromLonLat([-71.5388498, -16.3978613]),
                    zoom: 14
                })
            }),
            
        });
    }

  render() {
    return (
        <div className="row map-container">
            <div className="col-sm-12 map-block">
                <div id="map" className="map"></div>
            </div>
        </div>
    )
  }
}
