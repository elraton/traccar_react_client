import React, { Component } from 'react'
import Devices from './devices'
import Mapr from './map'

export default class Home extends Component {

    state = {
        openDevice: true,
        openHistory: true,
        devices: [],
        positions: [],
        deviceId: 0
    }

    constructor(props) {
        super(props)
        fetch(localStorage.getItem('baseurl') + '/devices', {
            method: 'GET',
            headers: {"Authorization": "Basic " + btoa(localStorage.getItem('user') + ":" + localStorage.getItem('pass'))},
        }).then((response) => response.json())
        .then((responseData) => {
            this.setState({ devices: responseData })
        })

        fetch(localStorage.getItem('baseurl') + '/server', {
            method: 'GET',
        }).then((response) => response.json())
        .then((responseData) => {
            
            fetch(localStorage.getItem('baseurl') + '/session?token='+localStorage.getItem('token'), {
                method: 'GET',
            }).then((response) => response.json())
            .then((response) => {
                const socket = new WebSocket(localStorage.getItem('baseurl').replace('http', 'ws') + '/socket');

                socket.onclose = (event) => {
                    console.log('socket closed');
                };

                socket.onmessage = (event) => {
                    var data = JSON.parse(event.data);
                    const devices = data.devices;
                    let devices_list = this.state.devices;
                    if (devices !== undefined) {
                        for (const xx of devices) {
                            let device = this.state.devices.find( x => x.uniqueId === xx.uniqueId);
                            devices_list[devices_list.indexOf(device)] = xx;
                        }
                        this.setState({ devices: devices_list })
                    }

                    const positions = data.positions;
                    let positions_list = this.state.positions
                    if (positions !== undefined) {
                        if (positions_list.length > 0 ) {
                            for (const xx of positions) {
                                positions_list = positions_list.filter(x => x.deviceId !== xx.deviceId);
                                positions_list.push(xx);
                                // this.state.deviceSelected = positions_list.find(x => x.deviceId == this.state.idSelected);
                            }
                            this.setState({ positions: positions_list })
                        } else {
                            this.setState({ positions: positions })
                        }
                    }
                };
            })
        })
        
    }

    toggleDevice = () => {
        this.setState({ openDevice: !this.state.openDevice })
    }
    
    toggleHistory = () => {
        this.setState({ openHistory: !this.state.openHistory })
    }
    
    render() {
        return (
            <div className="row home">
                <div className="col-sm-12 home_content">
                    <div className={this.state.openHistory ? 'row device-map-cont historyopen' : 'row device-map-cont historyclosed' }>
                        <div className={this.state.openDevice ? 'col-md-4 col-lg-3 devices open' : 'col-md-4 col-lg-3 devices closed' }>
                            <div className="title bg-dark"><span>Dispositivos</span></div>
                            <div className="v-separator">
                                <a className="arrow" onClick={this.toggleDevice}>
                                    <img src="/assets/arrow.png" />
                                </a>
                            </div>
                            <Devices className="device_cont"
                                show={this.state.openDevice}
                                devices={this.state.devices}
                                positions={this.state.positions}
                                /*outpositions={positionsReceived($event)}
                                outDeviceId={getDeviceId($event)}
                                outDevices={getDevices($event)}*/
                            ></Devices>
                        </div>
                        <div className="col bg-secondary map">
                            <Mapr devices={this.state.devices} positions={this.state.positions} deviceId={this.state.deviceId}></Mapr>
                        </div>
                    </div>
                    <div className={ this.state.openHistory ? 'row historial-cont  open' : 'row historial-cont closed' }>
                        <div className="col-sm-12 historial">
                            <div className="title bg-dark"><span>Reportes</span></div>
                            <div className="h-separator">
                                <a className="arrow" onClick={this.toggleHistory}>
                                    <img src="/assets/arrow.png" />
                                </a>
                            </div>
                            {/* <app-reports></app-reports> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
