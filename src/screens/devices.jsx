import React, { Component } from 'react'

export default class Devices extends Component {

    week_days = ['Dom', 'Lun', 'Mar', 'Mier', ' Jue', 'Vie', 'Sab']
    months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']

    state = {
        deviceSelected: null,
        showDeviceInfo: false,
        idSelected: 0,
    }

    formatHour = (date) => {
        return date.split('T')[0] + ' ' + date.split('T')[1].split('.')[0];
    }

    getSpeed = (speed) => {
        return (speed * 1.852).toFixed(2);
    }
    courseFormatter = (value) => {
        var courseValues = ['Norte', 'NorEste', 'Este', 'SurEste', 'Sur', 'SurOeste', 'Oeste', 'NorOeste'];
        return courseValues[Math.floor(value / 45)];
    }

    componentDidMount() {
        
    }

    getClasses = (dev) => {
        let clases = ''
        if (dev.id === this.state.idSelected) {
            clases = clases  + 'active'
        }
        if (dev.status === 'offline') {
            clases = clases  + 'offline'
        } else {
            clases = clases  + 'online'
        }
        return clases
    }

    getlastUpdate = (lastupdate) => {
        const right_now = new Date();
        const last_date = new Date(lastupdate);
        if (last_date.getDate() === right_now.getDate()) {
          const dif_hour = right_now.getHours() - last_date.getHours();
          const dif_mins = last_date.getMinutes() - right_now.getMinutes();
          if (dif_hour === 0) {
            if (dif_mins === 0) {
              return 'en linea';
            } else {
              return dif_mins + ' minutos';
            }
          } else {
            return dif_hour + ' horas ' + dif_mins + ' minutos';
          }
        } else {
          return this.week_days[last_date.getDay()] + ' ' + last_date.getDate() + ' ' + this.months[last_date.getMonth()] + ' ' + last_date.getFullYear();
        }
    }

    selectRow = (device) => {
        this.setState(
            {
                    deviceSelected: this.props.positions.find(x => x.deviceId == device.id),
                    idSelected: device.id,
                    showDeviceInfo: true,
            }
        )
        // this.outDeviceId.emit(this.idSelected);
    }

    render() {
    return (
        <div id="deviceContainer" className={this.props.show ? 'row open' : 'row closed'}>
            <div className="col">
                <div className="row">
                    <div className="col-sm-12 devices_list">
                        <div className="head row">
                            <div className="col">
                                <h2>Dispositivos</h2>
                            </div>
                        </div>
                        <div className="content">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col">Última Actualización</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    { this.props.devices.map( (dev, i) => 
                                        <tr key={i} onClick={ ()=>this.selectRow(dev) }>
                                            <td className={this.getClasses(dev)}>{ dev.name }</td>
                                            <td className={this.getClasses(dev)}>{ dev.status === 'offline' ? 'desconectado' : 'en linea' }</td>
                                            <td className={this.getClasses(dev)}>{ this.getlastUpdate(dev.lastUpdate) }</td>
                                        </tr>
                                    ) }
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="col-sm-12 device_info">
                        <div className="head row">
                            <div className="col">
                                <h2>Estado</h2>
                            </div>
                        </div>
                        <div className="content">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Valor</th>
                                    </tr>
                               </thead>
                               { this.state.deviceSelected !== null ? 
                                    <tbody>
                                        <tr>
                                            <td>Hora</td>
                                            <td>{ this.formatHour(this.state.deviceSelected.serverTime) }</td>
                                        </tr>
                                        <tr>
                                            <td>Latitud</td>
                                            <td>{ +this.state.deviceSelected.latitude.toFixed(7) }</td>
                                        </tr>
                                        <tr>
                                            <td>Longitud</td>
                                            <td>{ +this.state.deviceSelected.longitude.toFixed(7) }</td>
                                        </tr>
                                        <tr>
                                            <td>Precisión</td>
                                            <td>+/- { this.state.deviceSelected.accuracy } Km</td>
                                        </tr>
                                        <tr>
                                            <td>Altitud</td>
                                            <td>{ this.state.deviceSelected.altitude }</td>
                                        </tr>
                                        <tr>
                                            <td>Velocidad</td>
                                            <td>{ this.getSpeed(this.state.deviceSelected.speed) } Km/h</td>
                                        </tr>
                                        <tr>
                                            <td>Dirección</td>
                                            <td>{ this.courseFormatter(this.state.deviceSelected.course) }</td>
                                        </tr>
                                        <tr>
                                            <td>Encendido</td>
                                            <td>{ this.state.deviceSelected.attributes.ignition ? 'si' : 'no' }</td>
                                        </tr>
                                        <tr>
                                            <td>Distancia</td>
                                            <td>{ this.state.deviceSelected.attributes.distance } Km</td>
                                        </tr>
                                        <tr>
                                            <td>Distancia total</td>
                                            <td>{ this.state.deviceSelected.attributes.totalDistance } Km</td>
                                        </tr>
                                        <tr>
                                            <td>En movimiento</td>
                                            <td>{ this.state.deviceSelected.attributes.motion ? 'si' : 'no' }</td>
                                        </tr>
                                    </tbody> : <tbody></tbody>
                               }
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}
