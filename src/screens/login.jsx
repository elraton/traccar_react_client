import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'


export default class Login extends Component {

    state = {
        redirect: false
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const username  = event.target.username.value
        const password = event.target.password.value
        var data = new URLSearchParams()
        data.append('email', username)
        data.append('password', password)
    
        fetch(localStorage.getItem('baseurl') + '/session', {
          method: 'POST',
          headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
          body: data
        }).then((response) => response.json())
        .then((responseData) => {
          localStorage.setItem('user', username);
          localStorage.setItem('pass', password);
          localStorage.setItem('token', responseData.token);
          this.setState({ redirect: true })
        })
      }

  render() {
    const { redirect } = this.state;
    if (redirect) {
        return <Redirect to='/home'/>;
    }
    return (
    <div id="login" className="container">
        <div className="row-fluid">
          <div className="span12">
            <div className="login well well-small">
              <div className="center">
                <img src="http://placehold.it/250x100&text=Logo" alt="logo"/>
              </div>
                <form className="login-form" id="UserLoginForm" method="post" acceptCharset="utf-8" onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label>Email address</label>
                    <input type="text" className="form-control" name="username" id="username" placeholder="Usuario" />
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" id="password" placeholder="Password" />
                  </div>
                  <div className="control-group">
                    <input className="btn btn-primary btn-large btn-block" type="submit" value="Sign in" /> 
                  </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
